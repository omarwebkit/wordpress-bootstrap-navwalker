# Wordpress Bootstrap Navwalker

A custom WordPress Nav Walker Class to fully implement the Bootstrap 4 navigation style in a custom theme using the WordPress built in menu manager.

## Installation
Place download  class-wp-bootstrap-navwalker.php and upload/place in your WordPress theme folder _/wp-content/themes/my-theme/_

Open your WordPress themes functions.php file - _/wp-content/themes/my-theme/functions.php_ - and add the following code:

```

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );
```



You will also need to declare a new menu in your _functions.php_ file if one doesn't already exist.

```
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'THEMENAME' ),
) );
```

## Usage
Add or update any wp_nav_menu() functions in your theme (often found in header.php) to use the new walker by adding a 'walker' item to the wp_nav_menu args array.

```
wp_nav_menu( array(
    'theme_location'  => 'primary',
    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
    'container'       => 'div',
    'container_class' => 'collapse navbar-collapse',
    'container_id'    => 'bs-example-navbar-collapse-1',
    'menu_class'      => 'navbar-nav mr-auto',
    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
    'walker'          => new WP_Bootstrap_Navwalker(),
) );
```


## Authors and acknowledgment
This code author by 
https://github.com/wp-bootstrap/wp-bootstrap-navwalker/tree/v3-branch


## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
